/* 
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

Программа-заготовка для домашнего задания
*/

#ifndef HW_L3_DOMAIN_LAYER_H
#define HW_L3_DOMAIN_LAYER_H

#include "hw/l4_InfrastructureLayer.h"

const size_t MAX_NAME_LENGTH    = 50;
const size_t MIN_AMOUNT    = 0;

static std::vector<std::string> MetalTypes =
{
    "золото",
    "серебро",
    "медь"
};

bool isCorrectMetalType(const std::string&);

class Coin : public ICollectable
{
    std::string _common_name;
    std::string _special_name;
    std::string _metal_type;
    uint16_t _amount;
    uint16_t _amount_in_collection;
    
protected:
    bool invariant() const;

public:
    Coin() = delete;
    Coin(const Coin & p) = delete;

    Coin & operator = (const Coin & p) = delete;

    Coin(const std::string & _common_name, const std::string & _metal_type, uint16_t _amount, uint16_t _amount_in_collection);
    Coin(const std::string & _common_name, const std::string & _special_name, const std::string & _metal_type, uint16_t _amount, uint16_t _amount_in_collection);

    const std::string & getCommonName() const;
    const std::string & getSpecialName() const;
    const std::string & getMetalType() const;
    uint16_t       getAmount() const;
    uint16_t       getAmountInCollection() const;

    virtual bool   write(std::ostream& os) override;
};

class ItemCollector: public ACollector
{
public:
    virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
};

#endif // HW_L3_DOMAIN_LAYER_H
