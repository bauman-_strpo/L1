/* Программа-заготовка для домашнего задания
*/

#include "hw/l3_DomainLayer.h"

using namespace std;

bool isCorrectMetalType(const string& metalType)
{
    return std::find(MetalTypes.begin(), MetalTypes.end(), metalType) != MetalTypes.end();
}

bool Coin::invariant() const
{
    return _amount >= MIN_AMOUNT && _amount_in_collection >= MIN_AMOUNT 
        && isCorrectMetalType(_metal_type);
}

Coin::Coin(const std::string & common_name, const std::string & metal_type, uint16_t amount, uint16_t amount_in_collection)
    : _common_name(common_name), _special_name(""), _metal_type(metal_type), _amount(amount), _amount_in_collection(amount_in_collection)
{
    assert(invariant());
}

Coin::Coin(const std::string & common_name, const std::string & special_name, const std::string & metal_type, uint16_t amount, uint16_t amount_in_collection)
    : _common_name(common_name), _special_name(special_name), _metal_type(metal_type), _amount(amount), _amount_in_collection(amount_in_collection)
{
    assert(invariant());
}

const string & Coin::getCommonName() const
{
    return _common_name;
}

const string & Coin::getSpecialName() const
{
    return _special_name;
}

const string & Coin::getMetalType() const
{
    return _metal_type;
}

uint16_t Coin::getAmount() const
{
    return _amount;
}

uint16_t Coin::getAmountInCollection() const
{
    return _amount_in_collection;
}

bool   Coin::write(ostream& os)
{
    writeString(os, _common_name);
    writeString(os, _special_name);
    writeString(os, _metal_type);
    writeNumber(os, _amount);
    writeNumber(os, _amount_in_collection);

    return os.good();
}



shared_ptr<ICollectable> ItemCollector::read(istream& is)
{
    string   common_name = readString(is, MAX_NAME_LENGTH);
    string   special_name  = readString(is, MAX_NAME_LENGTH);
    string   metal_type = readString(is, MAX_NAME_LENGTH);
    uint16_t amount       = readNumber<uint16_t>(is);
    uint16_t amount_in_collection       = readNumber<uint16_t>(is);

    return std::make_shared<Coin>(common_name, special_name, metal_type, amount, amount_in_collection);
}
